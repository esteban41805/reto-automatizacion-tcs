# Reto Automatizacion portal Bancolombia Personas
## Automatizacion de funcionalidades web



<p align="center">
  <a href="https://serenity-bdd.github.io/theserenitybook/latest/index.html"> 
  <img src="https://serenity-bdd.info/wp-content/uploads/elementor/thumbs/serenity-bdd-pac9onzlqv9ebi90cpg4zsqnp28x4trd1adftgkwbq.png" title="Serenity"> 
  </a>
  <a href="https://cucumber.io/"> 
        <img src="src\test\resources\imagenes\Logos.png"> 
  </a>
  <a href="https://www.selenium.dev/"> 
  <img src="https://selenium-python.readthedocs.io/_static/logo.png" title="Selenium" > 
  </a>
  <a href="https://gradle.org/"> 
  <img src="https://gradle.org/images/gradle-knowledge-graph-logo.png?20170228" title="Gradle" > 
  </a> 
</p>

Portal Bancolombia Personas, se ha implementado una automatización web usando serenity + cucumber como su framework de automatización con el patrón de diseño Screenplay. Los casos de prueba implementados son:
## Casos de prueba automatizados
<ul>
    <li>Crédito para moto alto CC</li>
</ul>


<a href="https://www.bancolombia.com/personas">
    <img src="https://bogota.gov.co/sites/default/files/2020-04/logo-bancolombia-2.jpg">
</a>

## Almacenamiento datos 

| Descripción                                                                                                                                             | Nombre Archivo |
|:--------------------------------------------------------------------------------------------------------------------------------------------------------|----------------|
| Dentro del proyecto se encuentra un archivo Excel que guarda los datos solicitados cada que se realice una ejecución del Escenario SimularCreditoRunner | SimulacionesRealizadas.xls |

<img src="src\test\resources\imagenes\Direccion archivo.png">

##Problemas en iniciar la automatización

Si al ejecutar el escenario automatizado no inicia, por favor verificar que la versión de su navegador chrome sea 117.0 o actualice el chromedriver.exe

## Descarga del proyecto

- Usar una herramienta para conectar y descargar el proyecto como por ejemplo Gitbash.

Si no tiene configurado el git ejecutar:

```sh
git config --global user.name "Esteban Quintero"
git config --global user.email "esteban@gmail.com"
```
Una vez Configurado ejecutamos el siguiente comando para clonar el proyecto:
```sh
git clone https://gitlab.com/esteban41805/reto-automatizacion-tcs.git

```


# Licencias

MIT
Open-Source

**Free Software!**