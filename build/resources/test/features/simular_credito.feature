Feature: Yo como usuario quiero ingresar a finanza tus sueños
  y realizar una simulación de crédito

  Scenario Outline: Simular credito moto alto cilindraje
    Given Ingreso al portal de bancolombia personas
    When Realizo la simulacion de un credito para moto de alto cilindraje <compra> <prestamo> <cuotas>
    Then puedo guardar el resultado en una hoja de excel

    Examples:
      | compra | prestamo | cuotas |
      | Moto   | 18350355 | 48     |
      | Moto   | 16350355 | 72     |
      | Moto   | 13350355 | 24     |
