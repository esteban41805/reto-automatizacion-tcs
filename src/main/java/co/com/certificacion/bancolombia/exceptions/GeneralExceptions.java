package co.com.certificacion.bancolombia.exceptions;

public class GeneralExceptions extends AssertionError{
    public GeneralExceptions(String message, Throwable cause){
        super(message,cause);
    }
}
