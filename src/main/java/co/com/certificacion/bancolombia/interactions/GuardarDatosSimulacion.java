package co.com.certificacion.bancolombia.interactions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.Row;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static co.com.certificacion.bancolombia.userInterfaces.SufiPage.*;
import static net.serenitybdd.screenplay.Tasks.instrumented;

public class GuardarDatosSimulacion implements Interaction {

    @Override
    public <T extends Actor> void performAs(T actor) {

        DateFormat dateFormat = new SimpleDateFormat("MMM d, yyyy");
        String fecha = dateFormat.format(new Date());
        String valorMensual = CUOTA_MENSUAL_CALCULADA.resolveFor(actor).getText();
        String valorSeguro = SEGURO_VIDA_CALCULADA.resolveFor(actor).getText();
        String valorTotal = VALOR_TOTAL_CALCULADO.resolveFor(actor).getText();

        try {

            FileInputStream archivo = new FileInputStream("SimulacionesRealizadas.xls");

            HSSFWorkbook libro = new HSSFWorkbook(archivo);

            HSSFSheet hoja = libro.getSheet("Simulaciones");

            int numeroFilas = hoja.getLastRowNum();

            Row fila = hoja.createRow(numeroFilas + 1);

            fila.createCell(0).setCellValue(numeroFilas + 1);
            fila.createCell(1).setCellValue(fecha);
            fila.createCell(2).setCellValue(valorMensual);
            fila.createCell(3).setCellValue(valorSeguro);
            fila.createCell(4).setCellValue(valorTotal);

            FileOutputStream arch = new FileOutputStream("SimulacionesRealizadas.xls");
            libro.write(arch);

        } catch (Exception mensaje) {
            System.out.println("No funciona el archivo, verifique que lo tenga cerrado o la direccion");
        }
    }

    public static GuardarDatosSimulacion enArchivoExcel() {
        return instrumented(GuardarDatosSimulacion.class);
    }
}
