package co.com.certificacion.bancolombia.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static co.com.certificacion.bancolombia.userInterfaces.SufiPage.*;

public class ValorPagarMensual implements Question<Boolean> {
    @Override
    public Boolean answeredBy(Actor actor) {

        //Quitar signos a los resultados
        String cuotaMensual = CUOTA_MENSUAL_CALCULADA.resolveFor(actor).getText().replace("$","").replace(",","");
        String valorSeguro = SEGURO_VIDA_CALCULADA.resolveFor(actor).getText().replace("$","").replace(",","");
        String valorTotal = VALOR_TOTAL_CALCULADO.resolveFor(actor).getText().replace("$","").replace(",","");

        //Convierto en una lista que separa los elementos con puntos para quitar decimales
        String[] cuotaMensualSinDecimal = cuotaMensual.split("\\.");
        String[] valorSeguroSinDecimal = valorSeguro.split("\\.");
        String[] valorTotalSinDecimal = valorTotal.split("\\.");

        // Creo variables enteras para realizar operaciones. La posición [1] es de los decimales
        int cuotaMensualEntero = Integer.parseInt(cuotaMensualSinDecimal[0]);
        int valorSeguroEntero = Integer.parseInt(valorSeguroSinDecimal[0]);
        int valorTotalEntero = Integer.parseInt(valorTotalSinDecimal[0]);

        int sumaTotal = cuotaMensualEntero + valorSeguroEntero;

        //En caso de que los decimales sumen uno adicional se crea la segunda condición
        return sumaTotal == valorTotalEntero || (sumaTotal + 1) == valorTotalEntero;
    }

    public static ValorPagarMensual aPagar() {
        return new ValorPagarMensual();
    }
}
