package co.com.certificacion.bancolombia.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Scroll;
import net.serenitybdd.screenplay.actions.SendKeys;

import static co.com.certificacion.bancolombia.userInterfaces.CreditosPage.SIMULAR_CREDITO_MOTO_ALTO;
import static co.com.certificacion.bancolombia.userInterfaces.CreditosPage.TAB_VEHICULO;
import static co.com.certificacion.bancolombia.userInterfaces.PagPrincipal.*;
import static co.com.certificacion.bancolombia.userInterfaces.SufiPage.*;
import static net.serenitybdd.screenplay.Tasks.instrumented;

public class SolicitarCredito implements Task {

    private final String comprar;
    private final String valor;
    private final String cuotas;

    public SolicitarCredito(String comprar, String valor, String cuotas) {
        this.comprar = comprar;
        this.valor = valor;
        this.cuotas = cuotas;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(
                Click.on(CERRAR_PUBLICIDAD),
                Click.on(BTN_VER_MAS_CREDITOS),
                Click.on(TAB_VEHICULO),
                Click.on(SIMULAR_CREDITO_MOTO_ALTO),
                Scroll.to(SELECT_QUE_COMPRAR),
                Click.on(SELECT_QUE_COMPRAR),
                Click.on(OPTION_QUE_COMPRAR.of(comprar)),
                SendKeys.of(valor).into(INPUT_VALOR_CREDITO),
                Click.on(SELECT_CUANTAS_CUOTAS),
                Click.on(OPTION_CUANTAS_CUOTAS.of(cuotas)),
                Click.on(BTN_SIMULAR_CREDITO)
        );
    }

    public static SolicitarCredito paraMotoAltoCC(String comprar, String valor, String cuotas) {
        return instrumented(SolicitarCredito.class, comprar, valor, cuotas);
    }
}
