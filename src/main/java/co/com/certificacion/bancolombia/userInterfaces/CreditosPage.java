package co.com.certificacion.bancolombia.userInterfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class CreditosPage {

    public static final Target TAB_VEHICULO = Target.the("Seccion de Vehiculo")
            .located(By.id("tab-categoria-vehiculo"));

    public static final Target SIMULAR_CREDITO_MOTO_ALTO = Target.the("Boton para simular credito de moto alto cilindraje")
            .locatedBy("//*[contains(text(),'dito para moto de alto cilindraje')]/../../..//a[@class='btn btn-link link-simular']");
}
