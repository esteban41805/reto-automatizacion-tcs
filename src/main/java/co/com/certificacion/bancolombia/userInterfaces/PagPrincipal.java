package co.com.certificacion.bancolombia.userInterfaces;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;

@DefaultUrl("https://www.bancolombia.com/personas")

public class PagPrincipal extends PageObject {

    public static final Target CERRAR_PUBLICIDAD = Target.the("Boton cerrar la ventana emergente al ingresar")
            .locatedBy("//button[@id='close-modal-btn']");

    public static final Target BTN_VER_MAS_CREDITOS = Target.the("Boton ver más sobre 'Creditos, Fianancia lo que sueñas'")
            .locatedBy("//h3[contains(text(),'quieres y necesitas.')]/../../..//span[@class='btn btn-outline-brand btn-products']");
}
