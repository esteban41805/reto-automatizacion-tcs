package co.com.certificacion.bancolombia.userInterfaces;

import net.serenitybdd.screenplay.targets.Target;

public class SufiPage {

    public static final Target SELECT_QUE_COMPRAR = Target.the("Desplegable de opciones para seleccionar lo que se quiere comprar")
            .locatedBy("(//div[@class='custom-select']/select)[1]");

    public static final Target OPTION_QUE_COMPRAR = Target.the("Opcion para seleccionar que se va a comprar")
            .locatedBy("(//div[@class='custom-select']/select)[1]//option[contains(text(),'{0}')]");

    public static final Target INPUT_VALOR_CREDITO = Target.the("Campo para ingresar el valor del credito a simular")
            .locatedBy("//input[@name='valorSolicitado']");

    public static final Target SELECT_CUANTAS_CUOTAS = Target.the("Desplegable de opciones para seleccionar la cantidad de cuotas")
            .locatedBy("(//div[@class='custom-select']/select)[2]");

    public static final Target OPTION_CUANTAS_CUOTAS = Target.the("Opcion para seleccionar el numero de cuotas")
            .locatedBy("(//div[@class='custom-select']/select)[2]//option[contains(text(),'{0}')]");

    public static final Target BTN_SIMULAR_CREDITO = Target.the("Boton para simular el credito con los datos ingresados")
            .locatedBy("//*[@id='btnSubmit']");

    public static final Target CUOTA_MENSUAL_CALCULADA = Target.the("Valor calculado de las cuotas mensuales")
            .locatedBy("(//*[@class='val-number ng-binding'])[1]");

    public static final Target SEGURO_VIDA_CALCULADA = Target.the("Valor seguro de vida")
            .locatedBy("(//*[@class='val-number ng-binding'])[2]");

    public static final Target VALOR_TOTAL_CALCULADO = Target.the("Total de valor a pagar")
            .locatedBy("(//*[@class='result ng-binding'])[2]");



}
