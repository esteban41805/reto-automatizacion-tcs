package co.com.certificacion.bancolombia.runners;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        glue = "co.com.certificacion.bancolombia",
        features = "src/test/resources/features/simular_credito.feature",
        snippets = CucumberOptions.SnippetType.CAMELCASE
)
public class SimularCreditoRunner {
}
