package co.com.certificacion.bancolombia.stepsdefinitions;

import co.com.certificacion.bancolombia.exceptions.GeneralExceptions;
import co.com.certificacion.bancolombia.interactions.GuardarDatosSimulacion;
import co.com.certificacion.bancolombia.questions.ValorPagarMensual;
import co.com.certificacion.bancolombia.tasks.SolicitarCredito;
import co.com.certificacion.bancolombia.userInterfaces.PagPrincipal;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.Managed;
import org.openqa.selenium.WebDriver;

import static co.com.certificacion.bancolombia.utils.ConstantesExceptions.ERROR_CALCULO_INCORRECTOS;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;

public class SimularCreditoStepsDefinitions {

    @Managed(driver = "chrome")
    private WebDriver herBrowser;

    private final Actor actor = Actor.named("Esteban");

    private final PagPrincipal pagPrincipal = new PagPrincipal();

    @Before
    public void setUp() {
        actor.can(
                BrowseTheWeb.with(herBrowser)
        );
    }

    @Given("Ingreso al portal de bancolombia personas")
    public void ingresoAlPortalDeBancolombiaPersonas() {
        actor.wasAbleTo(
                Open.browserOn(pagPrincipal)
        );
    }

    @When("Realizo la simulacion de un credito para moto de alto cilindraje {} {} {}")
    public void realizoLaSimulacionDeUnCreditoParaMotoDeAltoCilindraje(String compra, String valor, String cuotas) {
        actor.attemptsTo(
                SolicitarCredito.paraMotoAltoCC(compra, valor, cuotas),
                GuardarDatosSimulacion.enArchivoExcel()
        );
    }

    @Then("puedo guardar el resultado en una hoja de excel")
    public void puedoGuardarElResultadoEnUnaHojaDeExcel() {
        actor.should(
                seeThat(ValorPagarMensual.aPagar()).orComplainWith(GeneralExceptions.class, ERROR_CALCULO_INCORRECTOS)
        );
    }
}
